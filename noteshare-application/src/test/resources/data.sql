-- insert two students, password is: "password" (without double quotes)
INSERT INTO user (id,age,email,first_name,last_name,password,username, favourite_id) VALUES (1,22,'email1@gmail.com','name1','surname1','$2a$10$6Vq3rgqUgsR.BM34lSHnheidBW.JfF07apgWYLhqWn6Yibr/c74Ce','student1',null);
INSERT INTO student (identification_number,university,user_id) VALUES (00001,'Bicocca',1);
INSERT INTO user (id,age,email,first_name,last_name,password,username, favourite_id) VALUES (2,23,'email2@gmail.com','name2','surname2','$2a$10$6Vq3rgqUgsR.BM34lSHnheidBW.JfF07apgWYLhqWn6Yibr/c74Ce','student2', 1);
INSERT INTO student (identification_number,university,user_id) VALUES (00002,'Polimi',2);

-- insert two experts, password is: "password" (without double quotes)
INSERT INTO user (id,age,email,first_name,last_name,password,username, favourite_id) VALUES (3,23,'email3@gmail.com','name3','surname3','$2a$10$6Vq3rgqUgsR.BM34lSHnheidBW.JfF07apgWYLhqWn6Yibr/c74Ce','expert1', 1);
INSERT INTO expert (expertise,years_of_experience,user_id) VALUES ('Security', 10, 3);
INSERT INTO user (id,age,email,first_name,last_name,password,username, favourite_id) VALUES (4,23,'email4@gmail.com','name4','surname4','$2a$10$6Vq3rgqUgsR.BM34lSHnheidBW.JfF07apgWYLhqWn6Yibr/c74Ce','expert2', 3);
INSERT INTO expert (expertise,years_of_experience,user_id) VALUES ('Software Engineering', 10, 4);

-- insert additional student for testing purposes
INSERT INTO user (id,age,email,first_name,last_name,password,username, favourite_id) VALUES (5,24,'email5@gmail.com','name5','surname5','$2a$10$6Vq3rgqUgsR.BM34lSHnheidBW.JfF07apgWYLhqWn6Yibr/c74Ce','student3', 2);
INSERT INTO student (identification_number,university,user_id) VALUES (00003,'Unimi',5);
INSERT INTO user (id,age,email,first_name,last_name,password,username, favourite_id) VALUES (6,30,'email6@gmail.com','name6','surname6','$2a$10$6Vq3rgqUgsR.BM34lSHnheidBW.JfF07apgWYLhqWn6Yibr/c74Ce','expert3', null);
INSERT INTO expert (expertise,years_of_experience,user_id) VALUES ('Maths', 5, 6);

-- insert subscriptions
INSERT INTO user_follow(follower_id,followed_id) VALUES(1,2);
INSERT INTO user_follow(follower_id,followed_id) VALUES(1,3);
INSERT INTO user_follow(follower_id,followed_id) VALUES(1,4);

-- insert notes
INSERT INTO note (id,content,subject,title,author_id) VALUES (1,'subject1','title1','content1', 1);
INSERT INTO note (id,content,subject,title,author_id) VALUES (2,'subject2','title2','content2', 1);
INSERT INTO note (id,content,subject,title,author_id) VALUES (3,'subject3','title3','content3', 2);

-- insert reviews
INSERT INTO review(fk_expert,fk_note,comment,value) VALUES (3,1,'comment1',10);
INSERT INTO review(fk_expert,fk_note,comment,value) VALUES (4,1,'comment2',7);
-- note1 will be published, note2 won't