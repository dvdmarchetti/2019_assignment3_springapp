package com.assignment.springapp.test.mocks;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockStudentSecurityContextFactory.class)
public @interface WithMockStudent {
    String value() default "student1";
}
