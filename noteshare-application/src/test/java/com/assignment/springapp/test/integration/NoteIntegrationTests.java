package com.assignment.springapp.test.integration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.assignment.springapp.models.Expert;
import com.assignment.springapp.models.Note;
import com.assignment.springapp.models.Review;
import com.assignment.springapp.models.Student;
import com.assignment.springapp.repositories.ExpertRepository;
import com.assignment.springapp.repositories.NoteRepository;
import com.assignment.springapp.repositories.ReviewRepository;
import com.assignment.springapp.repositories.StudentRepository;

/*
 * A H2 (temporary, in memory) database will be used
 *   its records can be found in test/resources/data.sql
 * At the end of each single test, the transaction will be rolled-back
 * 	 (it will go back to how it was in test/resources/data.sql)
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class NoteIntegrationTests {

	
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ExpertRepository expertRepository;
    @Autowired
    private ReviewRepository reviewRepository;
    
    //Test CRUD operations: Create, Read, Update, Delete
    
    @Test
    public void testAddNote() {
    	Student author = studentRepository.findById(1L).get();
    	Note note = new Note("Test1","Test1","Test1",author);
        Note savedNote = noteRepository.save(note);
        assertNotePersisted(savedNote);
    }

    @Test
    public void testUpdateNote() {
    	Note note = noteRepository.findById(1L).get();
    	note.setTitle("TestTitle");
        Note savedNote = noteRepository.save(note);
        assertEquals(note.getTitle(), savedNote.getTitle());
    }
    
    @Test
    public void testRemoveNote() {
    	Note note = noteRepository.findById(2L).get();
    	noteRepository.delete(note);
    	assertEquals(noteRepository.count(), 2);
    }
    
    @Test
    public void addReview() {
    	Note note = noteRepository.findById(Long.valueOf(1)).get();
    	Expert expert = expertRepository.findById(Long.valueOf(3)).get();
    	
    	Review review = new Review(note,expert,"Prova",10);
    	Review savedReview = this.reviewRepository.save(review);
    	
    	assertReviewPersisted(savedReview);
    }

    private void assertNotePersisted(Note input) {
        Optional<Note> note = noteRepository.findById(input.getId());
        assertThat(note.get()).isNotNull();
    }
    
    private void assertReviewPersisted(Review input) {
        Optional<Review> review = reviewRepository.findById(input.getId());
        assertThat(review.get()).isNotNull();
    }
    
    
}
