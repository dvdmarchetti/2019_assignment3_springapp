package com.assignment.springapp.test.mocks;

import com.assignment.springapp.security.StudentDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithMockStudentSecurityContextFactory implements WithSecurityContextFactory<WithMockStudent> {
    protected StudentDetailsService studentDetailsService;

    @Autowired
    public WithMockStudentSecurityContextFactory(StudentDetailsService studentDetailsService) {
        this.studentDetailsService = studentDetailsService;
    }

    @Override
    public SecurityContext createSecurityContext(WithMockStudent withMockStudent) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        UserDetails principal = this.studentDetailsService.loadUserByUsername(withMockStudent.value());
        context.setAuthentication(
                new UsernamePasswordAuthenticationToken(principal, principal.getUsername(), principal.getAuthorities())
        );

        return context;
    }
}
