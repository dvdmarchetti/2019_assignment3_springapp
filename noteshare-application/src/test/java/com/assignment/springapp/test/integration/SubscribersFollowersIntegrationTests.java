package com.assignment.springapp.test.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.assignment.springapp.models.Expert;
import com.assignment.springapp.models.Student;
import com.assignment.springapp.models.User;
import com.assignment.springapp.repositories.*;


@RunWith(SpringRunner.class)
@DataJpaTest
public class SubscribersFollowersIntegrationTests {
	
	 //Repositories declarations, will use H2 (temporary, in memory) database
     @Autowired 
     private StudentRepository studentRepository;
     @Autowired
     private ExpertRepository expertRepository;
     
     @Test
     public void addFavourite() {
    	 Student student1 = studentRepository.findById(Long.valueOf(1)).get();
    	 Student student2 = studentRepository.findById(Long.valueOf(2)).get();
    	 
    	 Expert expert1 = expertRepository.findById(Long.valueOf(3)).get();
    	 Expert expert2 = expertRepository.findById(Long.valueOf(4)).get();
    	 
    	 student1.setFavourite(student2);
    	 student2.setFavourite(expert1);
    	 
    	 expert1.setFavourite(expert2);
    	 expert2.setFavourite(student2);
    	 
    	Student savedStudent1 = studentRepository.save(student1);
    	Student savedStudent2 = studentRepository.save(student2);
    	Expert savedExpert1 = expertRepository.save(expert1);
    	Expert savedExpert2 = expertRepository.save(expert2); 
    	
    	assertEquals(savedStudent1.getFavourite(),savedStudent2);
    	assertEquals(savedStudent2.getFavourite(),savedExpert1);
    	assertEquals(savedExpert1.getFavourite(),savedExpert2);
    	assertEquals(savedExpert2.getFavourite(),savedStudent2);
     }
     
     @Test
     public void addFollwers() {
    	 Student student1 = studentRepository.findById(Long.valueOf(1)).get();
    	 Student student2 = studentRepository.findById(Long.valueOf(2)).get();
    	 
    	 Expert expert1 = expertRepository.findById(Long.valueOf(3)).get();
    	 Expert expert2 = expertRepository.findById(Long.valueOf(4)).get();
    	 
    	 student1.addFollower(student2);
    	 student1.addFollower(expert1);
    	 
    	 expert1.addFollower(student1);
    	 expert2.addFollower(expert1);
    	 
    	 
    	Student savedStudent1 = studentRepository.save(student1);
    	Student savedStudent2 = studentRepository.save(student2);
    	Expert savedExpert1 = expertRepository.save(expert1);
    	Expert savedExpert2 = expertRepository.save(expert2); 
    	
    	assertTrue(savedStudent1.getFollowers().contains(savedStudent2));
    	assertTrue(savedStudent2.getFollowings().contains(savedStudent1));
    	
    	assertTrue(savedStudent1.getFollowers().contains(savedExpert1));
    	assertTrue(savedExpert1.getFollowings().contains(savedStudent1));
    	
    	assertTrue(savedExpert1.getFollowers().contains(savedStudent1));
    	assertTrue(savedStudent1.getFollowings().contains(savedExpert1));
    	
    	assertTrue(savedExpert2.getFollowers().contains(savedExpert1));
    	assertTrue(savedExpert1.getFollowings().contains(savedStudent1));
     }
     
     
    
}