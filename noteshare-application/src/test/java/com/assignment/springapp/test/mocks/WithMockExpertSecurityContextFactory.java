package com.assignment.springapp.test.mocks;

import com.assignment.springapp.security.ExpertDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithMockExpertSecurityContextFactory implements WithSecurityContextFactory<WithMockExpert> {
    protected ExpertDetailsService expertDetailsService;

    @Autowired
    public WithMockExpertSecurityContextFactory(ExpertDetailsService expertDetailsService) {
        this.expertDetailsService = expertDetailsService;
    }

    @Override
    public SecurityContext createSecurityContext(WithMockExpert withMockExpert) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();

        UserDetails principal = this.expertDetailsService.loadUserByUsername(withMockExpert.value());
        context.setAuthentication(
                new UsernamePasswordAuthenticationToken(principal, principal.getUsername(), principal.getAuthorities())
        );

        return context;
    }
}
