package com.assignment.springapp.test.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.assignment.springapp.models.Expert;
import com.assignment.springapp.repositories.*;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ExpertIntegrationTests {
	
	 //Repositories declarations, will use H2 (temporary, in memory) database
     @Autowired 
     private ExpertRepository expertRepository;
     
     
     //Test CRUD operations: Create, Read, Update, Delete
     
     @Test
     public void testAddExpert() {
    	 Expert expert = new Expert("Edoardo","Silva","es@gmail.com","es","password",23,"10","Security");
     	 Expert savedExpert = expertRepository.save(expert);
         assertExpertPersisted(savedExpert);
     }

     @Test
     public void testUpdateExpert() {
    	 Expert expert = expertRepository.findById(3L).get();
     	 expert.setAge(99);
         Expert savedExpert = expertRepository.save(expert);
         assertEquals(expert.getAge(), savedExpert.getAge());
     }
     
     @Test
     public void testRemoveExpert() {
    	Expert expert = expertRepository.findById(6L).get();
    	expertRepository.deleteById(expert.getId());
     	assertEquals(2, expertRepository.count());
     }
     
     private void assertExpertPersisted(Expert input) {
         Optional<Expert> expert = expertRepository.findById(input.getId());
         assertThat(expert.get()).isNotNull();
     }
    
}
