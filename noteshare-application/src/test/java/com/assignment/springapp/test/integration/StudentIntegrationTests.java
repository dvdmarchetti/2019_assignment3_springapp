package com.assignment.springapp.test.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.assignment.springapp.models.Student;
import com.assignment.springapp.repositories.*;


@RunWith(SpringRunner.class)
@DataJpaTest
public class StudentIntegrationTests {
	
	 //Repositories declarations, will use H2 (temporary, in memory) database
     @Autowired 
     private StudentRepository studentRepository;
     
     
     //Test CRUD operations: Create, Read, Update, Delete
     
     @Test
     public void testAddStudent() {
    	 Student student = new Student("Edoardo","Silva","es@gmail.com","es","password",23,"12367","Bicocca");
     	 Student saved_student = studentRepository.save(student);
         assertStudentPersisted(saved_student);
     }

     @Test
     public void testUpdateStudent() {
    	 Student student = studentRepository.findById(2L).get();
     	 student.setAge(99);
         Student savedStudent = studentRepository.save(student);
         assertEquals(student.getAge(), savedStudent.getAge());
     }
     
     @Test
     public void testRemoveStudent() {
    	Student student = studentRepository.findById(5L).get();
     	studentRepository.deleteById(student.getId());
     	assertEquals(2, studentRepository.count());
     }
     
     private void assertStudentPersisted(Student input) {
         Optional<Student> student = studentRepository.findById(input.getId());
         assertThat(student.get()).isNotNull();
     }
    
}
