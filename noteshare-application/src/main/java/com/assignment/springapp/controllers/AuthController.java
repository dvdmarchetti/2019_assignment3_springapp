package com.assignment.springapp.controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {
    @GetMapping("/students/login")
    public String studentLogin(Model model, String error, String logout, String registered) {
        this.appendMessages(model, error, logout, registered);

        // If not yet authenticated return authentication form
        if (SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
            return "students/login";
        }

        // Else skip login
        return "redirect:/notes/read";
    }

    @GetMapping("/experts/login")
    public String expertLogin(Model model, String error, String logout, String registered) {
        this.appendMessages(model, error, logout, registered);

        // If not yet authenticated return authentication form
        if (SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
            return "experts/login";
        }

        // Else skip login
        return "redirect:/notes/read";
    }

    protected void appendMessages(Model model, String error, String logout, String registered) {
        if (error != null) {
            model.addAttribute("error", "The provided credentials don't match our records.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        if (registered != null) {
            model.addAttribute("message", "Your account has been created successfully.");
        }
    }
}
