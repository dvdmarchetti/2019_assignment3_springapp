package com.assignment.springapp.controllers;

import com.assignment.springapp.exceptions.ResourceNotFoundException;
import com.assignment.springapp.models.Note;
import com.assignment.springapp.models.Review;
import com.assignment.springapp.repositories.ExpertRepository;
import com.assignment.springapp.repositories.NoteRepository;
import com.assignment.springapp.repositories.ReviewRepository;
import com.assignment.springapp.security.StudentPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;


@Controller
@RequestMapping("/notes")
public class NoteController {
    protected final NoteRepository noteRepository;
    protected final ExpertRepository expertRepository;
    protected final ReviewRepository reviewRepository;

    @Autowired
    public NoteController(NoteRepository noteRepository, ExpertRepository expertRepository, ReviewRepository reviewRepository) {
        this.noteRepository = noteRepository;
        this.expertRepository = expertRepository;
        this.reviewRepository = reviewRepository;
    }

    @GetMapping(value = "/read")
    public String list(Model model) {
        model.addAttribute("published", this.noteRepository.findByPublishedTrue());
        model.addAttribute("unpublished", this.noteRepository.findByPublishedFalse());
        return "notes/list";
    }

    @GetMapping(value = "/my")
    public String listCreated(Model model, Authentication authentication) {
        StudentPrincipal principal = (StudentPrincipal) authentication.getPrincipal();

        model.addAttribute("published", this.noteRepository.findByAuthor(principal.getStudent()));
        model.addAttribute("message", "Showing all your notes (published and not published)");
        return "notes/list";
    }

    @GetMapping(value = "/{id}")
    public String show(@PathVariable Long id, Model model) {
        model.addAttribute("notes", this.noteRepository.findById(id).get());
        // TODO: create view for single not visualization
        return "notes/show";
    }

    @GetMapping(value = "/create")
    public String create(Model model) {
        model.addAttribute("note", new Note());
        return "notes/create";
    }

    @PostMapping(value = "")
    public String store(@ModelAttribute Note note, Authentication authentication) {
        StudentPrincipal principal = (StudentPrincipal) authentication.getPrincipal();
        note.setAuthor(principal.getStudent());

        this.noteRepository.save(note);
        return "redirect:/notes/my";
    }

    @GetMapping(value = "/{id}/edit")
    public String edit(@PathVariable Long id, Authentication authentication, Model model) {
        StudentPrincipal principal = (StudentPrincipal) authentication.getPrincipal();
        Optional<Note> _note = this.noteRepository.findById(id);

        if (!_note.isPresent()) {
            throw new ResourceNotFoundException();
        }

        Note note = _note.get();
        if (!note.getAuthor().equals(principal.getStudent())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        model.addAttribute("note", note);
        return "notes/update";
    }

    @PutMapping(value = "/{id}")
    public String update(@ModelAttribute Note note, Authentication authentication) {
        StudentPrincipal principal = (StudentPrincipal) authentication.getPrincipal();
        note.setAuthor(principal.getStudent());
        this.noteRepository.save(note);

        return "redirect:/notes/read";
    }

    @DeleteMapping(value = "/{id}")
    public String delete(@PathVariable Long id, Authentication authentication) {

        List<Review> reviews = this.reviewRepository.findByIdNoteId(id);

        for (Review r : reviews)
            this.reviewRepository.delete(r);

        this.noteRepository.deleteById(id);
        return "redirect:/notes/read";
    }

    @GetMapping(value = "/showNoteReviews/{noteId}")
    public String showNoteReviews(@PathVariable Long noteId, Model model) {
        model.addAttribute("reviews", this.reviewRepository.findByIdNoteId(noteId));
        return "reviews/list";
    }
}
