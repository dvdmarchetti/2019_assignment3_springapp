package com.assignment.springapp.controllers;

import com.assignment.springapp.exceptions.ResourceNotFoundException;
import com.assignment.springapp.models.Note;
import com.assignment.springapp.models.Review;
import com.assignment.springapp.models.ReviewId;
import com.assignment.springapp.repositories.NoteRepository;
import com.assignment.springapp.repositories.ReviewRepository;
import com.assignment.springapp.security.ExpertPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Controller
@RequestMapping(value = {"/notes/{noteId}/reviews", "/reviews"})
@PreAuthorize("hasAuthority('expert')")
public class ReviewsController {
    protected final NoteRepository noteRepository;
    protected final ReviewRepository reviewRepository;

    @Autowired
    public ReviewsController(NoteRepository noteRepository, ReviewRepository reviewRepository) {
        this.noteRepository = noteRepository;
        this.reviewRepository = reviewRepository;
    }

    @GetMapping(value = "/create")
    public String create(@PathVariable Long noteId, Model model, Authentication authentication) {
        ExpertPrincipal principal = (ExpertPrincipal) authentication.getPrincipal();
        Optional<Note> _note = this.noteRepository.findById(noteId);
        if (!_note.isPresent()) {
            throw new ResourceNotFoundException();
        }

        Note note = _note.get();
        Optional<Review> review = note.getReviews().stream().filter(x -> x.getExpert().equals(principal.getExpert())).findFirst();
        if (review.isPresent()) {
            return "redirect:/notes/" + note.getId() + "/reviews/edit";
        }

        model.addAttribute("note", note);
        model.addAttribute("review", new Review(note, principal.getExpert()));
        return "reviews/create";
    }

    @GetMapping(value = "/edit")
    public String edit(@PathVariable Long noteId, Authentication authentication, Model model) {
        ExpertPrincipal principal = (ExpertPrincipal) authentication.getPrincipal();
        Optional<Note> _note = this.noteRepository.findById(noteId);

        if (!_note.isPresent()) {
            throw new ResourceNotFoundException();
        }

        Note note = _note.get();
        Optional<Review> review = note.getReviews().stream().filter(x -> x.getExpert().equals(principal.getExpert())).findFirst();
        if (!review.isPresent()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        model.addAttribute("note", note);
        model.addAttribute("review", review);
        return "reviews/update";
    }

    @PutMapping(value = "")
    public String update(@ModelAttribute Review review, Authentication authentication) {
        review.setExpert(((ExpertPrincipal) authentication.getPrincipal()).getExpert());
        this.reviewRepository.save(review);

        return "redirect:/reviews/my";
    }

    @DeleteMapping(value = "/{noteId}")
    public String delete(@PathVariable Long noteId, @PathVariable Long expertId, Authentication authentication) {
        ExpertPrincipal principal = (ExpertPrincipal) authentication.getPrincipal();

        this.reviewRepository.deleteById(new ReviewId(noteId, principal.getExpert().getId()));
        return "redirect:/reviews/my";
    }

    @PostMapping(value = "")
    public String store(@PathVariable Long noteId, Authentication authentication, @ModelAttribute Review review) {
        ExpertPrincipal principal = (ExpertPrincipal) authentication.getPrincipal();
        Optional<Note> _note = this.noteRepository.findById(noteId);

        if (!_note.isPresent()) {
            throw new ResourceNotFoundException();
        }

        review.setExpert(principal.getExpert());
        review.setNote(_note.get());
        this.reviewRepository.save(review);

        return "redirect:/notes/read";
    }

    @GetMapping(value = "/my")
    public String listCreated(Model model, Authentication authentication) {
        ExpertPrincipal principal = (ExpertPrincipal) authentication.getPrincipal();

        model.addAttribute("reviews", this.reviewRepository.findByExpert(principal.getExpert()));
        return "reviews/list";
    }

}
