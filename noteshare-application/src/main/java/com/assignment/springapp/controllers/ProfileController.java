package com.assignment.springapp.controllers;

import com.assignment.springapp.exceptions.ResourceNotFoundException;
import com.assignment.springapp.models.Expert;
import com.assignment.springapp.models.Note;
import com.assignment.springapp.models.Review;
import com.assignment.springapp.models.Student;
import com.assignment.springapp.models.User;
import com.assignment.springapp.repositories.ExpertRepository;
import com.assignment.springapp.repositories.NoteRepository;
import com.assignment.springapp.repositories.ReviewRepository;
import com.assignment.springapp.repositories.StudentRepository;
import com.assignment.springapp.repositories.UserRepository;
import com.assignment.springapp.security.ExpertPrincipal;
import com.assignment.springapp.security.StudentPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/profile")
public class ProfileController {
    protected final UserRepository<User> userRepository;
    protected final StudentRepository studentRepository;
    protected final ExpertRepository expertRepository;
    protected final NoteRepository noteRepository;
    protected final ReviewRepository reviewRepository;

    @Autowired
    public ProfileController(UserRepository<User> userRepository,
                             StudentRepository studentRepository,
                             ExpertRepository expertRepository,
                             NoteRepository noteRepository,
                             ReviewRepository reviewRepository) {

        this.userRepository = userRepository;
        this.studentRepository = studentRepository;
        this.expertRepository = expertRepository;
        this.noteRepository = noteRepository;
        this.reviewRepository = reviewRepository;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "")
    public String show(Model model, Authentication authentication) {
        if (authentication.getPrincipal() instanceof ExpertPrincipal) {
            model.addAttribute("user", ((ExpertPrincipal) authentication.getPrincipal()).getExpert());
        } else if (authentication.getPrincipal() instanceof StudentPrincipal) {
            model.addAttribute("user", ((StudentPrincipal) authentication.getPrincipal()).getStudent());
        }

        return "profile/my";
    }

    @GetMapping(value = "/{username}")
    public String showPerson(@PathVariable String username, Model model) {
        Student student = this.studentRepository.findByUsername(username);
        if (student != null) {
            model.addAttribute("user", student);
            return "profile/my";
        }

        Expert expert = this.expertRepository.findByUsername(username);
        if (expert != null) {
            model.addAttribute("user", expert);
            return "profile/my";
        }

        throw new ResourceNotFoundException();
    }

    @GetMapping(value = "/{username}/subscribe")
    @PreAuthorize("isAuthenticated()")
    public String subscribe(@PathVariable String username, Model model, Authentication authentication, HttpServletRequest request) {
        UserDetails principal = (UserDetails) authentication.getPrincipal();

        User subscriber = this.userRepository.findByUsername(principal.getUsername());
        User subscribeTo = this.userRepository.findByUsername(username);

        this.validateSubscription(subscriber, subscribeTo);

        subscriber.getFollowings().add(subscribeTo);
        this.userRepository.save(subscriber);

        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/{username}/favourite")
    @PreAuthorize("isAuthenticated()")
    public String favourite(@PathVariable String username, Model model, Authentication authentication, HttpServletRequest request) {
        UserDetails principal = (UserDetails) authentication.getPrincipal();

        User subscriber = this.userRepository.findByUsername(principal.getUsername());
        User favouriteTo = this.userRepository.findByUsername(username);

        this.validateSubscription(subscriber, favouriteTo);

        subscriber.setFavourite(favouriteTo);
        favouriteTo.getFavourites().add(subscriber);

        this.userRepository.save(subscriber);

        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/showfavouritecontent")
    @PreAuthorize("isAuthenticated()")
    public String showFavouriteContent(Model model, Authentication authentication) throws Exception {
        User favourite = this.retrieveUserFromPrincipal(authentication).getFavourite();

        if (favourite instanceof Student) {
            model.addAttribute("notes", noteRepository.findByAuthor((Student) favourite));
        } else if (favourite instanceof Expert) {
            model.addAttribute("reviews", reviewRepository.findByExpert((Expert) favourite));
        }

        return "profile/favourite-content";
    }

    @GetMapping(value = "/showsubscriberscontent")
    @PreAuthorize("isAuthenticated()")
    public String showSubscribersContent(Model model, Authentication authentication) {
        this.retrieveUserFromPrincipal(authentication).getFollowings().forEach(user -> {
            if (user instanceof Student) {
                model.addAttribute("notes", noteRepository.findByPublishedTrueAndAuthor((Student) user));
            } else if (user instanceof Expert) {
                model.addAttribute("reviews", reviewRepository.findByExpertAndValueGreaterThan((Expert) user, 5));
            }
        });

        return "profile/subscribers-content";
    }

    @DeleteMapping(value = "/remove")
    @PreAuthorize("isAuthenticated()")
    public String removeAccount(Model model, Authentication authentication, HttpServletRequest request) {
        if (authentication.getPrincipal() instanceof ExpertPrincipal) {
            Expert expert = ((ExpertPrincipal) authentication.getPrincipal()).getExpert();
            
            List<Review> reviews = this.reviewRepository.findByExpert(expert);
            if (!reviews.isEmpty())
            	this.reviewRepository.deleteAll(reviews);
            
            this.expertRepository.delete(expert);
        } else if (authentication.getPrincipal() instanceof StudentPrincipal) {
            Student student = ((StudentPrincipal) authentication.getPrincipal()).getStudent();

            /*
            student.getNotes().forEach(note -> {
                this.reviewRepository.deleteByIdNoteId(note.getId());
                this.noteRepository.deleteById(note.getId());
            });
            */


            for (Note n : this.noteRepository.findByAuthor(student)) {
                List<Review> reviews = this.reviewRepository.findByNote(n);
                if (!reviews.isEmpty())
                    this.reviewRepository.deleteAll(reviews);
                //this.noteRepository.delete(n);
            }
            
            for (Note n : this.noteRepository.findByAuthor(student)) {
            	this.noteRepository.delete(n);
            }
            
            student = this.studentRepository.findByUsername(student.getUsername());
            //System.out.println(student);
            
            this.studentRepository.delete(student);
        }

        try {
            request.logout();
        } catch (ServletException e) {
            //
        }
        return "redirect:/";
    }

    @DeleteMapping(value = "/removeFavourite")
    @PreAuthorize("isAuthenticated()")
    public String removeFavourite(Authentication authentication, HttpServletRequest request) {
        User user = this.retrieveUserFromPrincipal(authentication);

        user.setFavourite(null);
        this.userRepository.save(user);

        return "redirect:" + request.getHeader("Referer");
    }

    @DeleteMapping(value = "/removeSubscription/{username}")
    @PreAuthorize("isAuthenticated()")
    public String removeSubscription(@PathVariable String username, Authentication authentication, HttpServletRequest request) {
        User user = this.retrieveUserFromPrincipal(authentication);
        User target = this.userRepository.findByUsername(username);

        user.getFollowings().remove(target);
        this.userRepository.save(user);

        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping(value = "/showsubscriptions")
    @PreAuthorize("isAuthenticated()")
    public String showSubscriptions(Authentication authentication, Model model) {
        User user = this.retrieveUserFromPrincipal(authentication);

        model.addAttribute("favourite", user.getFavourite());
        model.addAttribute("followers", user.getFollowers());
        model.addAttribute("followings", user.getFollowings());

        return "profile/subscriptions";
    }

    protected User retrieveUserFromPrincipal(Authentication authentication) {
        if (authentication.getPrincipal() instanceof ExpertPrincipal) {
            return ((ExpertPrincipal) authentication.getPrincipal()).getExpert();
        } else if (authentication.getPrincipal() instanceof StudentPrincipal) {
            return ((StudentPrincipal) authentication.getPrincipal()).getStudent();
        }

        return null;
    }

    private void validateSubscription(User subscriber, User subscribeTo) {
        if (subscriber == null || subscribeTo == null) {
            throw new ResourceNotFoundException();
        }

        if (subscriber == subscribeTo) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
