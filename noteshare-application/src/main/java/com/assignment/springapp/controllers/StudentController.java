package com.assignment.springapp.controllers;

import com.assignment.springapp.models.Student;
import com.assignment.springapp.repositories.StudentRepository;
import com.assignment.springapp.security.StudentPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/students")
public class StudentController {
    protected final PasswordEncoder passwordEncoder;
    protected final StudentRepository studentRepository;

    @Autowired
    public StudentController(PasswordEncoder passwordEncoder, StudentRepository studentRepository) {
        this.passwordEncoder = passwordEncoder;
        this.studentRepository = studentRepository;
    }

    @GetMapping(value = "/register")
    public String register(Model model) {
        model.addAttribute("student", new Student());
        return "students/register.html";
    }

    @PostMapping(value = "/register")
    public String store(@Valid @ModelAttribute Student student, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "students/register.html";
        }

        student.setPassword(this.passwordEncoder.encode(student.getPassword()));
        this.studentRepository.save(student);

        return "redirect:/students/login?registered";
    }

    @GetMapping(value = "/edit")
    public String showEditForm(Model model, Authentication authentication) {
        Student student = ((StudentPrincipal) authentication.getPrincipal()).getStudent();
        model.addAttribute("student", student);
        return "students/edit";
    }

    @PostMapping(value = "/edit/{id}")
    public String update(@ModelAttribute Student student, Authentication authentication) {
        Student original = ((StudentPrincipal) authentication.getPrincipal()).getStudent();

        if (student.getPassword() != null && student.getPassword().length() > 5) {
            student.setPassword(this.passwordEncoder.encode(student.getPassword()));
        } else {
            student.setPassword(original.getPassword());
        }
        this.studentRepository.save(student);

        return "redirect:/notes/read";
    }
}
