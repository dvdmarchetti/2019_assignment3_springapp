package com.assignment.springapp.controllers;

import com.assignment.springapp.models.Expert;
import com.assignment.springapp.repositories.ExpertRepository;
import com.assignment.springapp.security.ExpertPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/experts")
public class ExpertController {
    protected final PasswordEncoder passwordEncoder;
    protected final ExpertRepository expertRepository;

    @Autowired
    public ExpertController(PasswordEncoder passwordEncoder, ExpertRepository expertRepository) {
        this.passwordEncoder = passwordEncoder;
        this.expertRepository = expertRepository;
    }

    @GetMapping(value = "/register")
    public String register(Model model) {
        model.addAttribute("expert", new Expert());
        return "experts/register.html";
    }

    @PostMapping(value = "/register")
    public String store(@Valid @ModelAttribute Expert expert, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "experts/register.html";
        }

        expert.setPassword(this.passwordEncoder.encode(expert.getPassword()));
        this.expertRepository.save(expert);

        return "redirect:/experts/login";
    }

    @GetMapping(value = "/edit")
    public String showEditForm(Model model, Authentication authentication) {
        Expert expert = ((ExpertPrincipal) authentication.getPrincipal()).getExpert();
        model.addAttribute("expert", expert);
        return "experts/edit";
    }

    @PostMapping(value = "/edit/{id}")
    public String update(@ModelAttribute Expert expert, Authentication authentication) {
        Expert original = ((ExpertPrincipal) authentication.getPrincipal()).getExpert();

        if (expert.getPassword() != null && !expert.getPassword().isEmpty() && expert.getPassword().length() > 6) {
            expert.setPassword(this.passwordEncoder.encode(expert.getPassword()));
        } else {
            expert.setPassword(original.getPassword());
        }

        this.expertRepository.save(expert);
        return "redirect:/notes/read";
    }

}
