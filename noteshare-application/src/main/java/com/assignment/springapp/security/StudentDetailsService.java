package com.assignment.springapp.security;

import com.assignment.springapp.models.Student;
import com.assignment.springapp.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class StudentDetailsService implements UserDetailsService {
    @Autowired
    protected StudentRepository studentRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Student student = this.studentRepository.findByUsername(username);

        if (student == null) {
            throw new UsernameNotFoundException("User not found.");
        }

        return new StudentPrincipal(student);
    }
}
