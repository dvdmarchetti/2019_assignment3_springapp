package com.assignment.springapp.security;

import com.assignment.springapp.models.Expert;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ExpertPrincipal implements UserDetails {
    private static final long serialVersionUID = 1L;

    private final Expert expert;

    public ExpertPrincipal(Expert expert) {
        this.expert = expert;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("expert"));
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.expert.getPassword();
    }

    @Override
    public String getUsername() {
        return this.expert.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Expert getExpert() {
        return this.expert;
    }
}
