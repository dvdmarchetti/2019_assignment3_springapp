package com.assignment.springapp.security;

import com.assignment.springapp.models.Expert;
import com.assignment.springapp.repositories.ExpertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class ExpertDetailsService implements UserDetailsService {
    @Autowired
    protected ExpertRepository expertRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Expert expert = this.expertRepository.findByUsername(username);

        if (expert == null) {
            throw new UsernameNotFoundException("User not found.");
        }

        return new ExpertPrincipal(expert);
    }
}
