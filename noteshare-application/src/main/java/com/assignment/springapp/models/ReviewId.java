package com.assignment.springapp.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ReviewId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "fk_note")
    protected Long noteId;

    @Column(name = "fk_expert")
    protected Long expertId;

    public ReviewId() {
        //
    }

    public ReviewId(Long noteId, Long expertId) {
        this.noteId = noteId;
        this.expertId = expertId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(noteId, expertId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewId reviewId = (ReviewId) o;
        return Objects.equals(noteId, reviewId.noteId) &&
                Objects.equals(expertId, reviewId.expertId);
    }

    @Override
    public String toString() {
        return "ReviewId [noteId=" + noteId + ", expertId=" + expertId + "]";
    }

    public Long getNoteId() {
        return noteId;
    }

    public void setNoteId(Long noteId) {
        this.noteId = noteId;
    }

    public Long getExpertId() {
        return expertId;
    }

    public void setExpertId(Long expertId) {
        this.expertId = expertId;
    }
}
