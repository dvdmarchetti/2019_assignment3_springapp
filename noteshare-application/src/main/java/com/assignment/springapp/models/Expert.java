package com.assignment.springapp.models;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@PrimaryKeyJoinColumn(name = "user_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Expert extends User {
    @NotNull(message = "The expertise field cannot be empty.")
    private String expertise;

    @NotNull(message = "The years of experience field cannot be empty.")
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    @Column(name = "years_of_experience")
    private String yearsOfExperience;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "expert", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Review> reviews = new HashSet<>();

    public Expert() {
        //
    }

    //Constructor
    public Expert(
            @NotNull @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.") String firstName,
            @NotNull @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.") String lastName,
            @NotNull @Size(min = 1, message = "The email address must have at least one character.") String email,
            @NotNull(message = "The username field cannot be empty.") @Size(min = 1, message = "The username field must have at least one character.") String username,
            @NotNull(message = "The password field cannot be empty.") @Size(min = 6, message = "The password must be at least 6 characters long.") String password,
            @NotNull(message = "Please, provide a valid age.") @Min(value = 13, message = "You must be at least thirteen to sign up.") int age,
            @NotNull(message = "The expertise field cannot be empty.") @Size(min = 1, message = "The expertise field must have at least one character.") String expertise,
            @NotNull(message = "The yearsOfExperience field cannot be empty.") @Size(min = 1, message = "The yearsOfExperience field must be at least one.") String yearsOfExperience) {
        super(firstName, lastName, email, username, password, age);
        this.expertise = expertise;
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(String yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public String toString() {
        return "Expert [id=" + id + ", expertise=" + expertise + ", yearsOfExperience=" + yearsOfExperience + ", user=[" + super.toString() + "]]";
    }
    
    @PreRemove
    public void removeReviews() {
        this.reviews.forEach(review -> review.setExpert(null));
        this.reviews = null;
    }
}
