package com.assignment.springapp.models;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Objects;

@Entity
public class Review {
    @EmbeddedId
    private ReviewId id;

    @Min(0)
    @Max(10)
    private int value;

    private String comment;

    @ManyToOne
    @JoinColumn(name = "fk_note", insertable = false, updatable = false)
    private Note note;

    @ManyToOne
    @JoinColumn(name = "fk_expert", insertable = false, updatable = false)
    private Expert expert;

    public Review() {
        this.id = new ReviewId();
    }

    public Review(Note note, Expert expert) {
        this.id = new ReviewId(note.getId(), expert.getId());

        this.note = note;
        this.expert = expert;

        note.getReviews().add(this);
        expert.getReviews().add(this);
    }

    public Review(Note note, Expert expert, String comment, int value) {
        this(note, expert);

        this.comment = comment;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ReviewId getId() {
        return id;
    }

    public void setId(ReviewId id) {
        this.id = id;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        if (note != null) {
            this.note = note;
            this.getId().noteId = note.getId();
            note.getReviews().add(this);
        } else {
            this.note.getReviews().remove(this);
            this.note = null;
        }
    }

    public Expert getExpert() {
        return expert;
    }

    public void setExpert(Expert expert) {
        if (expert != null) {
            this.expert = expert;
            this.getId().expertId = expert.getId();
            expert.getReviews().add(this);
        } else {
            this.expert.getReviews().remove(this);
            this.expert = null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return Objects.equals(id, review.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Review [value=" + value + ", comment=" + comment + ", id=" + id + "]";
    }

    @PreRemove
    public void remove() {
        this.setNote(null);
        this.setExpert(null);

        this.id = null;
    }
}
