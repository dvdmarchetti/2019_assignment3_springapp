package com.assignment.springapp.models;

import com.assignment.springapp.models.constraints.UniqueUsername;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    protected Long id;

    @NotNull
    @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.")
    @Column(length = 60, name = "first_name")
    protected String firstName;

    @NotNull
    @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.")
    @Column(length = 60, name = "last_name")
    protected String lastName;

    @NotNull
    @Size(min = 1, message = "The email address must have at least one character.")
    protected String email;

    @NotNull(message = "The username field cannot be empty.")
    @Size(min = 1, message = "The username field must have at least one character.")
    @UniqueUsername(message = "This username is not available.")
    @Column(unique = true)
    protected String username;

    @NotNull(message = "The password field cannot be empty.")
    @Size(min = 6, message = "The password must be at least 6 characters long.")
    protected String password;

    @NotNull(message = "Please, provide a valid age.")
    @Min(value = 13, message = "You must be at least thirteen to sign up.")
    protected int age;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_follow",
            joinColumns = {@JoinColumn(name = "follower_id")},
            inverseJoinColumns = {@JoinColumn(name = "followed_id")})
    protected Set<User> followings = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "followings", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    protected Set<User> followers = new HashSet<>();

    //Favourite
    @ManyToOne
	@JoinColumn(name="favourite_id")
	private User favourite;
    @OneToMany(mappedBy="favourite", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<User> favourites = new HashSet<>();

    @PreRemove
    protected void detachFollowingAndFollowers() {
        this.followers.forEach(follower -> follower.getFollowings().remove(this));
        this.followings.forEach(following -> following.getFollowers().remove(this));
        this.favourite = null;
        this.favourites.forEach(favorite -> favorite.setFavourite(null));
    }

    public User() {
        //
    }

    public User(
			@NotNull @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.") String firstName,
			@NotNull @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.") String lastName,
			@NotNull @Size(min = 1, message = "The email address must have at least one character.") String email,
			@NotNull(message = "The username field cannot be empty.") @Size(min = 1, message = "The username field must have at least one character.") String username,
			@NotNull(message = "The password field cannot be empty.") @Size(min = 6, message = "The password must be at least 6 characters long.") String password,
			@NotNull(message = "Please, provide a valid age.") @Min(value = 13, message = "You must be at least thirteen to sign up.") int age) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.username = username;
		this.password = password;
		this.age = age;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<User> getFollowings() {
        return followings;
    }

    public void setFollowings(Set<User> followings) {
        this.followings = followings;
    }

    public void addFollowing(User follower) {
        this.followings.add(follower);
        follower.getFollowers().add(this);
    }

    public void removeFollowing(User follower) {
        this.followings.remove(follower);
        follower.getFollowers().remove(this);
    }

    public Set<User> getFollowers() {
        return followers;
    }

    public void setFollowers(Set<User> followers) {
        this.followers = followers;
    }

    public void addFollower(User follower) {
        this.followers.add(follower);
        follower.getFollowings().add(this);
    }

    public void removeFollower(User follower) {
        this.followers.remove(follower);
        follower.getFollowings().remove(this);
    }

    public User getFavourite() {
		return favourite;
	}

	public void setFavourite(User favourite) {
		this.favourite = favourite;
	}

	public Set<User> getFavourites() {
		return favourites;
	}

	public void setFavourites(Set<User> favourites) {
		this.favourites = favourites;
	}

	@Override
    public String toString() {
        return String.format("User[id=%d, firstName='%s', lastName='%s', age='%s', email='%s', username='%s', password=HIDDEN]",
                id, firstName, lastName, age, email, username);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
