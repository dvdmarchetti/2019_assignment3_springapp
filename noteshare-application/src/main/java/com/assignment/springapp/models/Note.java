package com.assignment.springapp.models;

import org.hibernate.annotations.Formula;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String subject;

    @NotNull
    private String content;

    // Author -- external table
    @NotNull
    @ManyToOne
    @JoinColumn(name = "author_id")
    private Student author;

    // Reviewers -- external table
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "note", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Review> reviews = new HashSet<>();

    @Formula("(SELECT COUNT(*) FROM review r WHERE r.fk_note = id)")
    int numberOfReviews = 0;

    @NotNull
    @Formula("(SELECT COUNT(*) >= 2 FROM review r WHERE r.fk_note = id)")
    boolean published = false;

    public Note() {
        //
    }

    public Note(String title, String content, String subject, Student author) {
        this.title = title;
        this.content = content;
        this.subject = subject;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Student getAuthor() {
        return author;
    }

    public void setAuthor(Student author) {
        this.author = author;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public int getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(int numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return Objects.equals(id, note.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Note [id=" + id + ", title=" + title + ", subject=" + subject + ", content=" + content + ", author="
                + author + ", reviewers=" + reviews + " published="
                + published + "]";
    }

    @PreRemove
    public void remove() {
        if (reviews != null) {
            this.reviews = null;
        }
        this.author.getNotes().remove(this);
        this.author = null;
    }
}
