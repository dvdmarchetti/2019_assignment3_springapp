package com.assignment.springapp.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "user_id")
public class Student extends User {
    @NotNull(message = "The identification number field cannot be empty.")
    @Size(min = 1, message = "The identification number field must have at least one character.")
    @Column(name = "identification_number")
    private String identificationNumber;

    @NotNull(message = "The university field cannot be empty.")
    @Size(min = 5, message = "The university field must have at least five characters.")
    private String university;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "author", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Note> notes = new ArrayList<>();

    public Student() {
        //
    }

    public Student(
            @NotNull @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.") String firstName,
            @NotNull @Size(min = 1, max = 60, message = "The first name must be between 1 and 60 characters long.") String lastName,
            @NotNull @Size(min = 1, message = "The email address must have at least one character.") String email,
            @NotNull(message = "The username field cannot be empty.") @Size(min = 1, message = "The username field must have at least one character.") String username,
            @NotNull(message = "The password field cannot be empty.") @Size(min = 6, message = "The password must be at least 6 characters long.") String password,
            @NotNull(message = "Please, provide a valid age.") @Min(value = 13, message = "You must be at least thirteen to sign up.") int age,
            @NotNull(message = "The identification number field cannot be empty.") @Size(min = 1, message = "The identification number field must have at least one character.") String identificationNumber,
            @NotNull(message = "The university field cannot be empty.") @Size(min = 5, message = "The university field must have at least five characters.") String university) {
        super(firstName, lastName, email, username, password, age);
        this.identificationNumber = identificationNumber;
        this.university = university;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public List<Note> getNotes() {
        return notes;
    }

    @Override
    public String toString() {
        return String.format("Student[id=%s, identificationNumber='%s', university='%s', user=[%s]]", id, identificationNumber, university, super.toString());
    }

    @PreRemove
    public void removeNotes() {
        this.notes.forEach(note -> note.setAuthor(null));
        this.notes = null;
    }
}
