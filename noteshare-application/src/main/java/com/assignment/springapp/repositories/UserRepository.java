package com.assignment.springapp.repositories;

import com.assignment.springapp.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository<T extends User> extends CrudRepository<T, Long> {
    T findByUsername(String username);

    boolean existsUserByUsername(String username);
}