package com.assignment.springapp.repositories;

import com.assignment.springapp.models.Expert;
import com.assignment.springapp.models.Note;
import com.assignment.springapp.models.Review;
import com.assignment.springapp.models.ReviewId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends CrudRepository<Review, ReviewId> {
    List<Review> findByExpert(Expert expert);

    List<Review> findByNote(Note note);

    List<Review> findByIdNoteId(Long id);

    List<Review> findByExpertAndValueGreaterThan(Expert expert, int value);
}
