package com.assignment.springapp.repositories;

import com.assignment.springapp.models.Note;
import com.assignment.springapp.models.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {
    List<Note> findByAuthor(Student student);

    List<Note> findByPublishedTrue();

    List<Note> findByPublishedFalse();

    List<Note> findByPublishedTrueAndAuthor(Student student);
}