package com.assignment.springapp.repositories;

import com.assignment.springapp.models.Expert;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpertRepository extends UserRepository<Expert> {
    //
}