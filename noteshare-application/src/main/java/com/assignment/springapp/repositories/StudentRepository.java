package com.assignment.springapp.repositories;

import com.assignment.springapp.models.Student;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends UserRepository<Student> {
    //
}