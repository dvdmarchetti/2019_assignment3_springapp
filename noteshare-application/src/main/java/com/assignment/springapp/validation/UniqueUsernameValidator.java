package com.assignment.springapp.validation;

import com.assignment.springapp.models.User;
import com.assignment.springapp.models.constraints.UniqueUsername;
import com.assignment.springapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {
    @Autowired
    protected UserRepository<User> userRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return this.userRepository == null || (value != null && !this.userRepository.existsUserByUsername(value));
    }
}
