package com.assignment.springapp.config;

import com.assignment.springapp.security.ExpertDetailsService;
import com.assignment.springapp.security.StudentDetailsService;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan("com.assignment.springapp.security")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public static PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public FilterRegistrationBean hiddenHttpMethodFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new HiddenHttpMethodFilter());
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/*"));
        return filterRegistrationBean;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/notes/**").permitAll()

                .and().csrf()

                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**", "/static/**", "/styles/**", "/scripts/**", "/images/**");
    }

    @Configuration
    @Order(2)
    public static class StudentConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Bean
        public UserDetailsService studentDetailsService() {
            return new StudentDetailsService();
        }

        @Bean
        protected DaoAuthenticationProvider studentAuthenticationProvider() {
            DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
            authenticationProvider.setUserDetailsService(studentDetailsService());
            authenticationProvider.setPasswordEncoder(encoder());
            return authenticationProvider;
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(studentDetailsService())
                    .passwordEncoder(encoder())
                    .and()
                    .authenticationProvider(studentAuthenticationProvider());
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers(
                            "/students*",
                            "/notes/my",
                            "/notes/create"
                    ).hasAuthority("student")
                    .anyRequest().permitAll();

            http.antMatcher("/students/**")
                    .formLogin()
                    .loginPage("/students/login")
                    .loginProcessingUrl("/students/login")
                    .defaultSuccessUrl("/notes/read");
        }
    }

    @Configuration
    @Order(1)
    public static class ExpertConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Bean
        public UserDetailsService expertDetailsService() {
            return new ExpertDetailsService();
        }

        @Bean
        protected DaoAuthenticationProvider expertAuthenticationProvider() {
            DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
            authenticationProvider.setUserDetailsService(expertDetailsService());
            authenticationProvider.setPasswordEncoder(encoder());
            return authenticationProvider;
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(expertDetailsService())
                    .passwordEncoder(encoder())
                    .and()
                    .authenticationProvider(expertAuthenticationProvider());
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers(
                            "/experts*",
                            "/notes/*/reviews/**",
                            "/reviews/*",
                            "/reviews/my"
                    ).hasAuthority("expert")
                    .anyRequest().permitAll();

            http.antMatcher("/experts/**")
                    .formLogin()
                    .loginPage("/experts/login")
                    .loginProcessingUrl("/experts/login")
                    .defaultSuccessUrl("/notes/read");
        }
    }
}
