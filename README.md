# NoteShare, a note-sharing platform
NoteShare is a note-sharing platform where students can share notes they take during lectures, and experts can review them. 

**Link to GitLab repository:** [click here](https://gitlab.com/dvdmarchetti/2019_assignment3_springapp)

**Link to Trello board:** [click here](https://trello.com/b/nQCPOYQR/assignment-3-pss)

## Group Components

 - Marchetti Davide (815990)
 - Porto Francesco (816042)
 - Silva Edoardo (816560)
 - Fedel Valeria (859984)

## Project description

Noteshare allows users to be registered either as Students or Experts. 

Students can create and share **notes** (for the sake of simplicity, we represent them as *simple strings*). Experts, on the other hand, have the role of assessing the quality of the documents submitted by Students, **before** they are officially published. In order to be published, a note needs *at least* **two reviews**.

Browsing the published notes does not require to be registered to Noteshare. In addition to that, the platform allows both Students and Experts to follow other users, in order to stay updated on the latest notes they create or review.

Other than Spring MVC and Hibernate, we have used  **Thymeleaf**  for dynamic page generation and  **Spring Data JPA**  in order to easily create Repositories for data access.

Some additional features include:

 - **Authentication** and **authorization** via Spring Security
 - **Password *hashing*** via BCrypt
 - **User profiles** are supported
 - **Docker Compose** support for faster deployment

Noteshare is based on [this Assignment 2](https://gitlab.com/mattiabrumana_/2019_assignment2_sharenotes) where a Requirements Elicitation Process was carried out. 

## Source-code responsibilities 
We have organized our source code into the following packages, which can be found under the **src/main/java** directory:

 - **com.assignment.springapp**: contains the *main class* for our webapp.
 - **com.assignment.springapp.config**: contains all the *configuration* Beans.
 - **com.assignment.springapp.models**: contains all the *entities* for our webapp. There are five entities: User, Student, Expert, Note and Review.
 - **com.assignment.springapp.models.constraints**: contains a custom built *validation annotation* to impose the uniqueness of a username during sign-up.
 - **com.assignment.springapp.controllers**: contains all the *controllers* for our webapp. There are controllers for each one of the aforementioned entities. There are also two controllers regulating user authentication and profile management.
 - **com.assignment.springapp.repositories**: contains all the *repositories* for our webapp. Since we used Spring Data JPA, these are only interface declarations.
 - **com.assignment.springapp.exceptions**: defines all the custom exceptions which can be used throughout the webapp.
 - **com.assignment.springapp.security**: defines two Spring Security Authentication Principals to achieve a complete separation between students and experts at authentication level.
 - **com.assignment.springapp.validation**: implements the interface contained in the **com.assignment.springapp.models.constraints** package with the validation logic. The only constraints provided has the aim of checking the uniqueness of a provided user name.
 
The views can be found in the **src/main/resources/templates** directory: they consist of HTML pages with Thymeleaf and Spring Security tags for dynamic page creation.

## Entity relationships
There are five main entities in the webapp: **User**, **Student**, **Expert**, **Note** and **Review**. 

According to the assignment's specifications, we have created the following relationships:

 - **is-a relationship**: **User-Student** and **User-Expert**. This relationship has been implemented by distinguishing the creation of two subcategories of users in the platform.
 - **one-to-many relationships**: **Student-Note**. Each note can be published by a single user.
 - **many-to-many:** **User-User**. By following a student, you will be able to see all his published notes. By following an expert, you will be able to see all its positive notes. (This could also be considered as a self-relationship.)
 - **self-relationship (one-to-many)**: **User-Favourite**. By selecting a student as favourite, you will be able to see all its notes (published **and** unpublished). By selecting an expert as favourite, you will be able to see all its reviews (positive **and** negative).
 - **many-to-many (double one-to-many)**: **Note-Review**. We decided to split the relationship into two one-to-many since we needed to add extra attributes (comment and value) to each Review. We therefore created a ReviewId class as an embeddable id, which serves as a composite primary key of the Review entity. This has proven to be the biggest challenge in the development of this webapp.

Please refer to the diagrams for further details.

## How to run this webapp
The only prerequisite is to have a working installation of Docker and Git.
First, you have to clone the NoteShare's repository through HTTP:
```
git clone https://gitlab.com/dvdmarchetti/2019_assignment3_springapp.git
```
or SSH:
```
git clone git@gitlab.com:dvdmarchetti/2019_assignment3_springapp.git
```

Move to the cloned directory:
```
cd 2019_assignment3_springapp/
```


Then, run the webapp with the following command:
```
docker-compose up -d --build
```

The app will be available at [http://localhost:8080].

The first boot will take quite a while because the application's dependencies have to be downloaded and the executable itself generated. After the first build the dependencies will be cached allowing to speed up the docker image build process.

## How to use NoteShare
As soon as you connect to the website, you will be presented with a user-selection screen. Here you will be able to choose between Students or Experts, but you can also choose to just browse the notes.

If you choose Students or Experts, the first thing to do is the registration. After that, you will be able to login with the username and password you just chose.

When you login, you will first be presented with the list of **published notes** (that is, the notes with at least 2 reviews). You can view all your notes, including not published ones, by using ***Show my notes***. 

Notes can be added by clicking on ***Create Note***. You will NOT be able to see it in the list of published notes, since they have no reviews yet. But you can see them from ***Show my notes***. For each of your notes, you will also be able to ***edit*** or ***delete*** them.

Let us now focus on ***Show my profile***: that's right, each user has its own profile! Other than a recap of one's information (name, surname, etc.), from here you will be able to either ***update*** or ***delete*** your profile.

Another important button is ***Show my subscriptions***: each user, either Student or Expert, can in fact follow other users (in order to stay up to date with their latest work). There are two types of subscriptions:

 - **Follower**: each user can follow one or more users. If you follow a Student, you will be able to see all his/her ***published* notes** in a convenient way. If you follow an Expert, you will be able to see all his/her ***positive* reviews** (with a score >= 6/10).
 - **Favourite**: each user can follow AT MOST one user. If you favourite a Student, you will be able to see all his/her notes (including ***non-published*** ones). If you favourite an Expert, you will be able to see all his/her reviews (including ***negative*** ones). [Note: the AT MOST constraint was just a way to diversify the two relationships a bit more]

From the ***Expert*** point of view it works in a similar way to the Student, but with the difference that you will be able to ***add/edit/remove reviews***. Experts can also use the subscription feature of NoteShare.

*Additional note:* We understand that managing all these things may require a lot of time for evaluation purposes. We recommend using the .sql script **data.sql** under src/tests for faster data insertion. You can connect to the mysql server via MySql Workbench (user is: **springuser** and password is: **ThePassword**). Of course feel free to add additional Users/Notes etc. as you please!

## State of work
For the first deadline we wanted to have **both** the front-end and the back-end working, albeit with limited functionalities. We were in fact able to achieve these goals: 

- Spring MVC is **fully working** (we still have to implement more views, i.e. for Experts)
- Hibernate correctly maps entities to our database, and CRUD operations via Spring Data JPA are **correctly executed** (at least on Notes)

~~In the next few weeks, we will focus on completing all the views and adding more features to NoteShare. We are also taking a look at Docker Compose (for easier deployment), Sessions (e.g. allow a user to remove a note if he created it), and more...~~ All these features have been implemented as-of the final deadline!